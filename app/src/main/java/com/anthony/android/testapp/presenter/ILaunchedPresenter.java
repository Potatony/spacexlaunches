package com.anthony.android.testapp.presenter;

import com.anthony.android.testapp.view.LaunchedView;


public interface ILaunchedPresenter {
    void bind(LaunchedView view);

    void unbind();

    void loadData();
}
