package com.anthony.android.testapp.model;

public interface ILaunchedInteractor {
    void getData(DataListener listener);
}
