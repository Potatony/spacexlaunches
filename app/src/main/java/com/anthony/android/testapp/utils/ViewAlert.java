package com.anthony.android.testapp.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.anthony.android.testapp.R;
import com.squareup.picasso.Picasso;

public class ViewAlert {

    public void showDialog(final Context activity, String rocket, String image_url, final String article_url, final String video_url) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_layout);

        TextView text = (TextView) dialog.findViewById(R.id.alert_textview);
        text.setText(rocket);
        ImageView image = (ImageView) dialog.findViewById(R.id.alert_image);
        Picasso.with(activity).load(image_url).fit().centerCrop().placeholder(R.mipmap.ic_launcher).into(image);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_alert_video);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (video_url != null && !video_url.isEmpty()) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(video_url));
                    activity.startActivity(i);
                } else {
                    Toast.makeText(activity, "No video provided.", Toast.LENGTH_LONG).show();
                }
            }
        });

        Button dialogButton2 = (Button) dialog.findViewById(R.id.btn_alert_article);
        dialogButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (article_url != null && !article_url.isEmpty()) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(article_url));
                    activity.startActivity(i);
                } else {
                    Toast.makeText(activity, "No article provided.", Toast.LENGTH_LONG).show();
                }
            }
        });

        dialog.show();

    }
}