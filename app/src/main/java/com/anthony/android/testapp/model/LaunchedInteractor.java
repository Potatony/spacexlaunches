package com.anthony.android.testapp.model;

import com.anthony.android.testapp.apiclient.APIClient;


public class LaunchedInteractor implements ILaunchedInteractor {
    @Override
    public void getData(DataListener listener) {
        APIClient.getInstance().call(listener);
    }
}
