package com.anthony.android.testapp.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.anthony.android.testapp.R;
import com.anthony.android.testapp.model.ILaunchedInteractor;
import com.anthony.android.testapp.model.LaunchedInteractor;
import com.anthony.android.testapp.model.POJO.Launch;
import com.anthony.android.testapp.presenter.LaunchedPresenter;
import com.anthony.android.testapp.utils.ViewAlert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements LaunchedView {

    private LaunchedPresenter presenter;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private Button button_try;
    private LaunchAdapter mAdapter;
    private List<Launch> launchList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ILaunchedInteractor interactor = new LaunchedInteractor();
        presenter = new LaunchedPresenter(interactor);
        init();
        presenter.bind(this);
        try {
            if (isNetworkAvailable()) {
                presenter.loadData();
            } else {
                presenter.onError("No internet connection");
                button_try.setVisibility(View.VISIBLE);
            }
        } catch (InterruptedException | IOException e) {
            presenter.onError("No internet connection");
            button_try.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.unbind();
    }

    @Override
    public void updateUi(List<Launch> launches) {
        launchList.clear();
        launchList.addAll(launches);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void showProgress() {
        recyclerView.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        button_try.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        recyclerView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
        button_try.setVisibility(View.GONE);
    }

    @Override
    public void showToast(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    void init() {
        button_try = (Button) findViewById(R.id.button_try);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mAdapter = new LaunchAdapter(launchList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {

                        ViewAlert alert = new ViewAlert();
                        alert.showDialog(MainActivity.this, launchList.get(position).getRocket().rocket_name, launchList.get(position).getLinks().mission_patch, launchList.get(position).getLinks().article_link, launchList.get(position).getLinks().video_link);

                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }
                })
        );
        button_try.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (isNetworkAvailable()) {
                        presenter.loadData();
                    } else {
                        presenter.onError("No internet connection");
                        button_try.setVisibility(View.VISIBLE);
                    }
                } catch (InterruptedException | IOException e) {
                    presenter.onError("No internet connection");
                    button_try.setVisibility(View.VISIBLE);
                }
            }
        });
    }


    public boolean isNetworkAvailable() throws InterruptedException, IOException {
        String command = "ping -c 1 google.com";
        return (Runtime.getRuntime().exec(command).waitFor() == 0);
    }
}
