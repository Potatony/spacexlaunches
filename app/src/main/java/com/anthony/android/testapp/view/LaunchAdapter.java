package com.anthony.android.testapp.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.anthony.android.testapp.R;
import com.anthony.android.testapp.model.POJO.Launch;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class LaunchAdapter extends RecyclerView.Adapter<LaunchAdapter.MyViewHolder> {

    private List<Launch> launchList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView rocket_name, details, launch_date_unix;
        public ImageView mission_patch;

        public MyViewHolder(View view) {
            super(view);
            rocket_name = (TextView) view.findViewById(R.id.name);
            details = (TextView) view.findViewById(R.id.description);
            launch_date_unix = (TextView) view.findViewById(R.id.date);
            mission_patch = (ImageView) view.findViewById(R.id.patch);
        }
    }


    public LaunchAdapter(List<Launch> launchList, Context context) {
        this.launchList = launchList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.launch_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Launch launch = launchList.get(position);
        holder.rocket_name.setText(launch.getRocket().rocket_name);
        if (launch.getDetails() != null) {
            holder.details.setText(launch.getDetails());
        } else {
            holder.details.setText("No description");
        }
        long dv = Long.valueOf(launch.getLaunch_date_unix()) * 1000;
        Date df = new java.util.Date(dv);
        String date = new SimpleDateFormat("MM.dd.yy, HH:mm").format(df);
        holder.launch_date_unix.setText(date);
        Picasso.with(context).load(launch.getLinks().mission_patch).fit().centerCrop().placeholder(R.mipmap.ic_launcher).into(holder.mission_patch);
    }

    @Override
    public int getItemCount() {
        return launchList.size();
    }
}