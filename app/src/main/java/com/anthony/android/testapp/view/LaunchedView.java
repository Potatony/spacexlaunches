package com.anthony.android.testapp.view;


import com.anthony.android.testapp.model.POJO.Launch;

import java.util.List;

public interface LaunchedView {
    void updateUi(List<Launch> launches);

    void showProgress();

    void hideProgress();

    void showToast(String error);
}
