package com.anthony.android.testapp.presenter;

import com.anthony.android.testapp.model.DataListener;
import com.anthony.android.testapp.model.ILaunchedInteractor;
import com.anthony.android.testapp.model.POJO.Launch;
import com.anthony.android.testapp.view.LaunchedView;

import java.util.List;

public class LaunchedPresenter implements ILaunchedPresenter, DataListener {
    LaunchedView view;
    private ILaunchedInteractor interactor;

    public LaunchedPresenter(ILaunchedInteractor interactor) {
        this.interactor = interactor;
    }

    @Override
    public void bind(LaunchedView view) {
        this.view = view;
    }

    @Override
    public void unbind() {
        view = null;
    }

    @Override
    public void loadData() {
        view.showProgress();
        interactor.getData(this);
    }

    @Override
    public void onSuccess(List<Launch> launches) {
        view.updateUi(launches);
        view.hideProgress();
    }

    @Override
    public void onError(String error) {
        view.showToast(error);
        view.hideProgress();
    }
}
