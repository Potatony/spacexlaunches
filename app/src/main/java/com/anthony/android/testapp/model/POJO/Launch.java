package com.anthony.android.testapp.model.POJO;


public class Launch {
    public Links links;
    public String launch_date_unix;
    public String details;
    public Rocket rocket;

    public class Links {
        public String mission_patch;
        public String article_link;
        public String video_link;

        public Links(String mission_patch, String article_link, String video_link) {
            this.mission_patch = mission_patch;
            this.article_link = article_link;
            this.video_link = video_link;
        }
    }

    public class Rocket {
        public String rocket_name;

        public Rocket(String rocket_name) {
            this.rocket_name = rocket_name;
        }
    }

    public Launch(Links links, String launch_date_unix, String details, Rocket rocket) {
        this.links = links;
        this.launch_date_unix = launch_date_unix;
        this.details = details;
        this.rocket = rocket;
    }

    public Links getLinks() {
        return links;
    }

    public String getLaunch_date_unix() {
        return launch_date_unix;
    }

    public String getDetails() {
        return details;
    }

    public Rocket getRocket() {
        return rocket;
    }
}
