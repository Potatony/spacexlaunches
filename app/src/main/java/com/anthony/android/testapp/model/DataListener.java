package com.anthony.android.testapp.model;

import com.anthony.android.testapp.model.POJO.Launch;

import java.util.List;

public interface DataListener {

    void onSuccess(List<Launch> launches);

    void onError(String error);

}
