package com.anthony.android.testapp.apiclient;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.anthony.android.testapp.model.DataListener;
import com.anthony.android.testapp.model.POJO.Launch;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class APIClient {
    private static APIClient instance = null;

    public RequestQueue requestQueue;

    private APIClient(Context context) {
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    public static synchronized APIClient getInstance(Context context) {
        if (null == instance)
            instance = new APIClient(context);
        return instance;
    }

    public static synchronized APIClient getInstance() {
        if (null == instance) {
            throw new IllegalStateException(APIClient.class.getSimpleName() +
                    " (singleton)");
        }
        return instance;
    }


    public void call(final DataListener listener) {
        String url = "https://api.spacexdata.com/v2/launches?year=2017";
        StringRequest request = new StringRequest
                (Request.Method.GET, url, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Gson gson = new Gson();
                        Type type = new TypeToken<List<Launch>>() {
                        }.getType();
                        List<Launch> launchList = gson.fromJson(response, type);
                        listener.onSuccess(launchList);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onError("An error occurred while loading data.");
                        Log.e("ERROR", "ERROR OCCURRED");

                    }
                });

        requestQueue.add(request);
    }
}
