package com.anthony.android.testapp.application;

import android.app.Application;

import com.anthony.android.testapp.apiclient.APIClient;

public class SXApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        APIClient.getInstance(this);
    }


}
